var express=require("express");

var urlShortener=require("./url-shortener")
//This module handles all of the logic for 
//shortening ,temp storage in an array,validation,
//and returning string for the express app to use for redirection


var app=express();
var port=process.env.PORT||3000;

app.use('/',express.static('public'))

app.get('/invalid',function(req,res){
  res.send("This url does not exist, sorry not sorry.")
})//invalid link

app.get('/lib',function(req,res){
  res.send(urlShortener.showCurrent())
})//library of current links

app.get('/:url',function(req,res){
  var url=req.params.url;
  var host='https://'+req.host
  
  res.redirect(urlShortener.getUrl(url,host))
})//redirects to a valid url in an array

app.get('/new/:url*',function(req,res){
  var url=req.url.slice(5);
  var host='https://'+req.host;
  res.json(urlShortener.newLink(url,host))
})//create a new link with the '/new' route



app.listen(port,function(){
  console.log("Express server is listening on port "+port)
})
