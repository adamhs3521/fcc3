var validator = require('valid-url');
var urlArray=[
    {short_url: "https://fcc-3-adams3521.c9users.io/nblc",
    original_url: "https://www.google.com"}];


function newLink(url,current){
 var obj={short_url:current+'/'+linkGen(),
                  original_url:url};
 
  (validator.isUri(url))? 
      urlArray.push(obj):obj={Url:"Invalid"};
  
  return obj
}  

function getUrl(url,current){
 var result;
 urlArray.forEach(function(val){
   if(val.short_url.slice(val.short_url.length-4)==url){
     result=val.original_url;
    }
  })
   return result|| current+'/invalid'
}  
function showCurrent(){
  return urlArray;
}

function linkGen(){
        var charArray="abcdefghijklmnopqrstuvwxyz0123456789"
        .split("").map((a)=>a);
        var i=0, res=[];
        
        while(i<4){
            var rand=Math.floor(Math.random()*charArray.length)
            res.push(charArray[rand]);
            i++
        }
        return( res.join(''));
    }
module.exports={newLink:newLink,getUrl:getUrl,showCurrent:showCurrent}        